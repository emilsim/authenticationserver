package bg.sofia.uni.fmi.mjt.authentication.commands;

import bg.sofia.uni.fmi.mjt.authentication.exceptions.InvalidParametersException;
import bg.sofia.uni.fmi.mjt.authentication.user.Users;

public class UpdateUserCommand implements Command {

    private static final int MIN_NUMBER_OF_ARGUMENTS = 3;
    private static final int SESSION_ID_PARAMETER = 2;
    private static final int MAX_NUMBER_OF_ARGUMENTS = 11;

    @Override
    public String execute(Users users, String[] tokens)
            throws InvalidParametersException {
        StringBuilder result = new StringBuilder();
        if (areParametersValid(tokens)) {

            final String sessionId = tokens[SESSION_ID_PARAMETER];
            for (int i = 0; i < tokens.length; i++) {
                switch (tokens[i]) {
                    case "--new-username" :
                        result.append(users.updateUsersUsername(sessionId,
                                                                tokens[i + 1]));
                        break;
                    case "--new-first-name" :
                        result.append(users.updateUsersFirstName(sessionId,
                                                                 tokens[i + 1]));
                        break;
                    case "--new-last-name" :
                        result.append(users.updateUsersLastName(sessionId,
                                                                tokens[i + 1]));
                        break;
                    case "--new-email" :
                        result.append(users.updateUsersEmail(sessionId,
                                                             tokens[i + 1]));
                        break;
                    default :
                        break;
                }
            }
            users.saveUsersToFile();
            return result.toString();
        }
        return "Invalid parameters!";
    }

    private boolean areParametersValid(String[] tokens)
            throws InvalidParametersException {

        if (tokens.length >= MIN_NUMBER_OF_ARGUMENTS
                && tokens.length < MAX_NUMBER_OF_ARGUMENTS) {
            return true;
        }
        throw new InvalidParametersException(
                "You have entered invalid number of parameters!");
    }

}
