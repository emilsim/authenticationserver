package bg.sofia.uni.fmi.mjt.authentication.exceptions;

public class InvalidPasswordException extends Exception {

    private static final long serialVersionUID = -8804070532982872192L;

    public InvalidPasswordException(String message) {

        super(message);
    }

}
