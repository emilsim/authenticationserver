package bg.sofia.uni.fmi.mjt.authentication.commands;

import bg.sofia.uni.fmi.mjt.authentication.exceptions.InvalidParametersException;
import bg.sofia.uni.fmi.mjt.authentication.user.Users;

public class ResetPasswordCommand implements Command {

    private static final int NUMBER_OF_PARAMETERS = 7;
    private static final int USERNAME_PARAMETER = 2;
    private static final int OLD_PASSWORD_PARAMETER = 4;
    private static final int NEW_PASSWORD_PARAMETER = 6;

    @Override
    public String execute(Users users, String[] tokens)
            throws InvalidParametersException {

        if (areParametersValid(tokens)) {
            final String username = tokens[USERNAME_PARAMETER];
            final String oldPassword = tokens[OLD_PASSWORD_PARAMETER];
            final String newPassword = tokens[NEW_PASSWORD_PARAMETER];
            return users.resetPassword(username, oldPassword, newPassword);
        }
        return "You have entered invalid parameters!";
    }

    private boolean areParametersValid(String[] tokens)
            throws InvalidParametersException {

        if (tokens.length == NUMBER_OF_PARAMETERS) {
            return true;
        }
        throw new InvalidParametersException(
                "You have entered invalid number of parameters!");
    }

}
