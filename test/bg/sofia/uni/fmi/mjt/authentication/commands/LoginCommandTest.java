package bg.sofia.uni.fmi.mjt.authentication.commands;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import bg.sofia.uni.fmi.mjt.authentication.exceptions.InvalidParametersException;
import bg.sofia.uni.fmi.mjt.authentication.user.Users;

import org.junit.Before;
import org.junit.Test;

public class LoginCommandTest {

    private static final String COMMAND = "register";
    private static final String USERNAME_PARAMETER = "--username";
    private static final String PASSWORD_PARAMETER = "--password";
    private static final String SESSION_ID_PARAMETER = "--session-id";

    private static final String USERNAME = "example";
    private static final String PASSWORD = "exa0mple";
    private static final String SESSION_ID = "exa0mple";

    private Users users;
    private LoginCommand loginCommand;

    @Before
    public void setup() {

        users = mock(Users.class);
        loginCommand = new LoginCommand();
    }

    @Test
    public void givenExistingUsernameAndValidPasswordWhenExecuteIsInvokedThenAuthenticateUser()
            throws InvalidParametersException {

        final String[] tokens = {COMMAND, USERNAME_PARAMETER, USERNAME,
                PASSWORD_PARAMETER, PASSWORD};
        loginCommand.execute(users, tokens);
        final int numberOfInvokes = 1;
        verify(users, times(numberOfInvokes)).authenticateUser(USERNAME,
                                                               PASSWORD);
    }

    @Test(expected = InvalidParametersException.class)
    public void givenInvalidNumberOfParametersWhenExecuteIsInvokedThenThrowException()
            throws InvalidParametersException {

        final String[] tokens = {COMMAND};
        loginCommand.execute(users, tokens);
    }

    @Test
    public void givenValidSessionIdWhenExecuteIsInvokedThenAuthenticateUser()
            throws InvalidParametersException {

        final String[] tokens = {COMMAND, SESSION_ID_PARAMETER, SESSION_ID};
        loginCommand.execute(users, tokens);
        final int numberOfInvokes = 1;
        verify(users, times(numberOfInvokes)).authenticateUser(SESSION_ID);
    }

}
