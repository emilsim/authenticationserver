package bg.sofia.uni.fmi.mjt.authentication;

import bg.sofia.uni.fmi.mjt.authentication.enums.CommandType;
import bg.sofia.uni.fmi.mjt.authentication.exceptions.ExceptionLogger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class AuthenticationClient {

    private static final String HOST = "127.0.0.1";
    private static final int PORT = 8080;
    private InputScanner inputScanner;
    private ClientRunnable clientRunnable;
    private boolean isDisconnected;

    public AuthenticationClient(InputScanner inputScanner) {

        this.inputScanner = inputScanner;
        this.isDisconnected = false;
    }

    public void run() {

        try (Socket socket = new Socket(HOST, PORT);
                PrintWriter socketWriter = new PrintWriter(
                        socket.getOutputStream(), true);
                BufferedReader socketReader = new BufferedReader(
                        new InputStreamReader(socket.getInputStream()))) {

            createClientRunnable(socket, socketReader);

            while (!isDisconnected) {
                String clientInput = inputScanner.getString();
                String[] tokens = clientInput.split(" ");
                String command = tokens[0];

                if (CommandType.DISCONNECT.getText().equals(command)) {
                    isDisconnected = true;
                    clientRunnable.disconnect();
                    System.out.println("You have disconnected from the server!");
                }
                executeCommand(command, clientInput, socketWriter);
            }
        } catch (UnknownHostException exception) {
            ExceptionLogger.logException(exception);
            throw new IllegalStateException(
                    "There is no host " + HOST
                            + " . Connection to server cannot be established",
                    exception.getCause());
        } catch (IOException exception) {
            ExceptionLogger.logException(exception);
            throw new IllegalStateException("Connection to server on " + HOST
                    + ":" + PORT + " cannot be established",
                    exception.getCause());
        }
    }

    private void createClientRunnable(Socket socket,
            BufferedReader socketReader) {

        clientRunnable = new ClientRunnable(socket, socketReader);
        Thread readerThread = new Thread(clientRunnable);
        readerThread.start();
    }

    private void executeCommand(String command, String clientInput,
            PrintWriter socketWriter) {

        if (!isCommandExisting(command)) {
            System.out.println("Invalid command " + command);
            return;
        }

        socketWriter.println(clientInput);
    }

    private boolean isCommandExisting(String userCommand) {

        for (CommandType command : CommandType.values()) {
            if (command.getText().equals(userCommand)) {
                return true;
            }
        }
        return false;
    }

}
