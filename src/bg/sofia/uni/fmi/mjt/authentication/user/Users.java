package bg.sofia.uni.fmi.mjt.authentication.user;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

public class Users {

    private static final String FILE_NAME = "resources/users/users.bin";
    private static final int TIME_TO_LIVE = 10;
    private Map<String, User> users;
    private Map<String, Session> usernameSessions;
    private Map<String, Session> sessionIdSessions;

    public Users() {

        users = new HashMap<>();
        usernameSessions = new HashMap<>();
        sessionIdSessions = new HashMap<>();
    }

    public synchronized void addUser(String username, String password,
            String firstName, String lastName, String email) {

        User user = new User(username, password, firstName, lastName, email);
        users.put(username, user);
        saveUsersToFile();
    }

    public boolean isUserRegistered(String username) {

        return users.containsKey(username);
    }

    public boolean isUserAuthenticated(String username) {

        Session session = usernameSessions.get(username);
        if (session != null && session.isSessionExpired()) {
            usernameSessions.remove(username);
            String sessionId = session.getUniqueId();
            sessionIdSessions.remove(sessionId);
        }
        return usernameSessions.containsKey(username);
    }

    public void saveUsersToFile() {

        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                new FileOutputStream(FILE_NAME));) {
            objectOutputStream.writeInt(users.size());
            for (User user : users.values()) {
                objectOutputStream.writeObject(user);
            }
        } catch (IOException e) {
            System.out.println("Cannot open file " + FILE_NAME
                    + " to save users");
        }
    }

    public void readUsersFromFile() {

        try (ObjectInputStream objectInputStream = new ObjectInputStream(
                new FileInputStream(FILE_NAME))) {
            int counter = 0;
            int numberOfUsers = objectInputStream.readInt();
            while (counter < numberOfUsers) {
                User user = (User) objectInputStream.readObject();
                users.put(user.getUsername(), user);
                counter++;
            }
        } catch (IOException e) {
            System.out.println("Cannot open file " + FILE_NAME
                    + " to save users");
        } catch (ClassNotFoundException e) {
            System.out.println("Cannot read users from file " + FILE_NAME);
        }
    }

    public synchronized void removeUser(String username) {

        Session session = usernameSessions.remove(username);
        if (session != null) {
            sessionIdSessions.remove(session.getUniqueId());
        }
        users.remove(username);
        saveUsersToFile();
    }

    public synchronized String authenticateUser(String username,
            String password) {

        User user = users.get(username);
        if (user == null) {
            return "You have entered an invalid username";
        }
        if (user.isValidPassword(password)) {
            if (usernameSessions.containsKey(user.getUsername())) {
                Session session = usernameSessions.remove(user.getUsername());
                sessionIdSessions.remove(session.getUniqueId());
            }
            Session userSession = new Session(username, TIME_TO_LIVE);
            usernameSessions.put(username, userSession);
            sessionIdSessions.put(userSession.getUniqueId(), userSession);
            return "Session id: " + userSession.getUniqueId()
                    + "\nTime to live: " + userSession.getTimeToLive();
        } else {
            return "You have entered an invalid password";
        }
    }

    public synchronized String authenticateUser(String sessionId) {

        Session session = sessionIdSessions.get(sessionId);
        if (session != null) {
            sessionIdSessions.remove(sessionId);
            String username = session.getUsername();
            usernameSessions.remove(username);

            Session userSession = new Session(username, TIME_TO_LIVE);
            usernameSessions.put(username, userSession);
            sessionIdSessions.put(userSession.getUniqueId(), userSession);
            return "Session id: " + userSession.getUniqueId()
                    + "\nTime to live: " + userSession.getTimeToLive();
        } else {
            return "Session id: " + sessionId + " doesn't exist!";
        }
    }

    public synchronized String deauthenticateUser(String sessionId) {

        if (!sessionIdSessions.containsKey(sessionId)) {
            return "Session id: " + sessionId + " doesn't exist!";
        }
        Session session = sessionIdSessions.remove(sessionId);
        usernameSessions.remove(session.getUsername());
        return "You have successfully logged out from the System!";
    }

    public synchronized String resetPassword(String username,
            String oldPassword, String newPassword) {

        if (!usernameSessions.containsKey(username)) {
            return "You should loggin into the system to change password!";
        }
        User user = users.get(username);
        if (!user.isValidPassword(oldPassword)) {
            return "You have entered an invalid old password!";
        }
        user.setPassword(newPassword);
        return "Successfully change password!";
    }

    public synchronized String updateUsersUsername(String sessionId,
            String newUsername) {

        if (!isUserAuthenticatedSessionId(sessionId)) {
            return "You should loggin into the system to change username!\n";
        }
        Session session = sessionIdSessions.get(sessionId);
        String username = session.getUsername();
        User user = users.get(username);
        users.remove(username);
        user.setUsername(newUsername);
        users.put(user.getUsername(), user);
        return "Successfully change username to " + newUsername + "\n";
    }

    public synchronized String updateUsersFirstName(String sessionId,
            String newFirstName) {

        if (!isUserAuthenticatedSessionId(sessionId)) {
            return "You should loggin into the system to change first name!\n";
        }
        Session session = sessionIdSessions.get(sessionId);
        String username = session.getUsername();
        User user = users.get(username);
        user.setFirstName(newFirstName);
        return "Successfully change first name to " + newFirstName + "\n";
    }

    public synchronized String updateUsersLastName(String sessionId,
            String newLastName) {

        if (!isUserAuthenticatedSessionId(sessionId)) {
            return "You should loggin into the system to change last name!\n";
        }
        Session session = sessionIdSessions.get(sessionId);
        String username = session.getUsername();
        User user = users.get(username);
        user.setLastName(newLastName);
        return "Successfully change last name to " + newLastName + "\n";
    }

    public synchronized String updateUsersEmail(String sessionId,
            String newEmail) {

        if (!isUserAuthenticatedSessionId(sessionId)) {
            return "You should loggin into the system to change email!\n";
        }
        Session session = sessionIdSessions.get(sessionId);
        String username = session.getUsername();
        User user = users.get(username);
        user.setEmail(newEmail);
        return "Successfully change last email to " + newEmail + "\n";
    }

    private boolean isUserAuthenticatedSessionId(String sessionId) {

        Session session = sessionIdSessions.get(sessionId);
        if (session != null && session.isSessionExpired()) {
            sessionIdSessions.remove(sessionId);
            String username = session.getUsername();
            usernameSessions.remove(username);
        }
        return sessionIdSessions.containsKey(sessionId) ? true : false;
    }
}
