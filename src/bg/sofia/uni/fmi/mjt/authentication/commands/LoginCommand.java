package bg.sofia.uni.fmi.mjt.authentication.commands;

import bg.sofia.uni.fmi.mjt.authentication.exceptions.InvalidParametersException;
import bg.sofia.uni.fmi.mjt.authentication.user.Users;

public class LoginCommand implements Command {

    private static final int NUMBER_OF_PARAMETERS_SESSION = 3;
    private static final int NUMBER_OF_PARAMETERS_USERNAME = 5;
    private static final int USERNAME_PARAMETER = 2;
    private static final int PASSWORD_PARAMETER = 4;
    private static final int SESSION_PARAMETER = 2;

    @Override
    public String execute(Users users, String[] tokens)
            throws InvalidParametersException {

        String username = null;
        if (areParametersValid(tokens)) {
            if (tokens.length == NUMBER_OF_PARAMETERS_USERNAME) {
                username = tokens[USERNAME_PARAMETER];
                final String password = tokens[PASSWORD_PARAMETER];
                return users.authenticateUser(username, password);
            } else {
                final String sessionId = tokens[SESSION_PARAMETER];
                return users.authenticateUser(sessionId);
            }
        }
        return "Not valid parameters!";
    }

    private boolean areParametersValid(String[] tokens)
            throws InvalidParametersException {

        if (tokens.length == NUMBER_OF_PARAMETERS_SESSION
                || tokens.length == NUMBER_OF_PARAMETERS_USERNAME) {
            return true;
        }
        throw new InvalidParametersException(
                "You have entered invalid number of parameters");
    }

}
