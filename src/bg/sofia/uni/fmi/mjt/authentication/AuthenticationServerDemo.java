package bg.sofia.uni.fmi.mjt.authentication;

public class AuthenticationServerDemo {

    public static void main(String[] args) {

        AuthenticationServer server = new AuthenticationServer();
        server.start();
    }

}
