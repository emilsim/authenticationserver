package bg.sofia.uni.fmi.mjt.authentication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

import org.junit.Before;
import org.junit.Test;

public class ClientRunnableTest {

    private static final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private BufferedReader socketReader;
    private Socket socket;

    @Before
    public void setup() {

        socketReader = mock(BufferedReader.class);
        socket = mock(Socket.class);
    }

    @Test
    public void givenSocketAndSocketReaderWithOneMessageFromServerWhenRunIsInvokedThenPrintMessageAndTerminateRunnable()
            throws IOException {

        when(socketReader.readLine()).thenReturn("Answer");
        when(socket.isClosed()).thenReturn(false).thenReturn(true);
        ClientRunnable clientRunnable = new ClientRunnable(socket,
                socketReader);
        clientRunnable.run();
        verify(socket, times(2)).isClosed();
        verify(socketReader, times(1)).readLine();
    }

    @Test
    public void givenSocketAndSocketReaderThatThrowsIOExceptionWhenRunIsInvokedThenLogException()
            throws IOException {

        System.setOut(new PrintStream(outContent));
        when(socketReader.readLine()).thenThrow(IOException.class);
        when(socket.isClosed()).thenReturn(false).thenReturn(true);
        ClientRunnable clientRunnable = new ClientRunnable(socket,
                socketReader);
        clientRunnable.run();
        final String expected = "Cannot read from the socket!\nclient socket is closed, "
                + "stop waiting for server messages\n";
        assertEquals(expected, outContent.toString());
    }

    @Test
    public void givenSocketAndSocketReaderAndThreadThatSetsDisconnectWhenRunIsInvokedThenPrintMessageAndTerminateRunnable()
            throws IOException {

        System.setOut(new PrintStream(outContent));
        when(socketReader.readLine()).thenReturn("Answer");
        when(socket.isClosed()).thenReturn(false).thenReturn(false);
        ClientRunnable clientRunnable = new ClientRunnable(socket,
                socketReader);
        new Thread(new Runnable() {

            @Override
            public void run() {
                clientRunnable.disconnect();
            }

        }).start();
        clientRunnable.run();
        final String expected = "client socket is closed, stop waiting for server messages";
        assertTrue(outContent.toString().contains(expected));

    }

}
