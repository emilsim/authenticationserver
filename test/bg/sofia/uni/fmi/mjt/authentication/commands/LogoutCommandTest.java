package bg.sofia.uni.fmi.mjt.authentication.commands;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import bg.sofia.uni.fmi.mjt.authentication.exceptions.InvalidParametersException;
import bg.sofia.uni.fmi.mjt.authentication.user.Users;

import org.junit.Before;
import org.junit.Test;

public class LogoutCommandTest {

    private static final String COMMAND = "logout";
    private static final String SESSION_ID_PARAMETER = "--session-id";
    private static final String SESSION_ID = "example";
    private Users users;
    private LogoutCommand logoutCommand;

    @Before
    public void setup() {

        users = mock(Users.class);
        logoutCommand = new LogoutCommand();
    }

    @Test
    public void givenSessionIdWhenExecuteIsInvokedThenDeauthenticateUserWithThatSessionId()
            throws InvalidParametersException {

        final String[] tokens = {COMMAND, SESSION_ID_PARAMETER, SESSION_ID};
        logoutCommand.execute(users, tokens);
        final int numberOfInvokes = 1;
        verify(users, times(numberOfInvokes)).deauthenticateUser(SESSION_ID);
    }

    @Test(expected = InvalidParametersException.class)
    public void givenInvalidNumberOfParametersWhenExecuteIsInvokedThenThrowException()
            throws InvalidParametersException {

        final String[] tokens = {COMMAND};
        logoutCommand.execute(users, tokens);
    }

}
