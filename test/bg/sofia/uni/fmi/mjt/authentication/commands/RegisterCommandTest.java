package bg.sofia.uni.fmi.mjt.authentication.commands;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import bg.sofia.uni.fmi.mjt.authentication.exceptions.InvalidParametersException;
import bg.sofia.uni.fmi.mjt.authentication.user.Users;

import org.junit.Before;
import org.junit.Test;

public class RegisterCommandTest {

    private static final String COMMAND = "register";
    private static final String USERNAME = "example";
    private static final String PASSWORD = "exa0mple";
    private static final String FIRST_NAME = "Example";
    private static final String LAST_NAME = "Example";
    private static final String EMAIL = "example@abv.bg";
    private static final String USERNAME_PARAMETER = "--username";
    private static final String PASSWORD_PARAMETER = "--password";
    private static final String FIRST_NAME_PARAMETER = "--first-name";
    private static final String LAST_NAME_PARAMETER = "--last-name";
    private static final String EMAIL_PARAMETER = "--email";

    private Users users;
    private RegisterCommand registerCommand;

    @Before
    public void setup() {

        users = mock(Users.class);
        registerCommand = new RegisterCommand();
    }

    @Test
    public void givenValidUserDataWhenExecuteIsInvokedThenAddUserToUsers()
            throws InvalidParametersException {

        final String[] userData = {COMMAND, USERNAME_PARAMETER, USERNAME,
                PASSWORD_PARAMETER, PASSWORD, FIRST_NAME_PARAMETER, FIRST_NAME,
                LAST_NAME_PARAMETER, LAST_NAME, EMAIL_PARAMETER, EMAIL};
        RegisterCommand registerCommand = new RegisterCommand();
        registerCommand.execute(users, userData);
        final int numberOfInvokes = 1;
        verify(users, times(numberOfInvokes)).addUser(USERNAME, PASSWORD,
                                                      FIRST_NAME, LAST_NAME,
                                                      EMAIL);
    }

    @Test(expected = InvalidParametersException.class)
    public void givenUserDataAndInvalidUsernameWhenExecuteIsInvokedThenThrowExeption()
            throws InvalidParametersException {

        final String username = "user name";
        final String[] userData = {COMMAND, username, PASSWORD, FIRST_NAME,
                LAST_NAME, EMAIL};
        registerCommand.execute(users, userData);
    }

    @Test(expected = InvalidParametersException.class)
    public void givenUserDataAndInvalidPasswordWhenExecuteIsInvokedThenThrowExeption()
            throws InvalidParametersException {

        final String password = "pass-word";
        final String[] userData = {COMMAND, USERNAME_PARAMETER, USERNAME,
                PASSWORD_PARAMETER, password, FIRST_NAME_PARAMETER, FIRST_NAME,
                LAST_NAME_PARAMETER, LAST_NAME, EMAIL_PARAMETER, EMAIL};
        registerCommand.execute(users, userData);
    }

    @Test(expected = InvalidParametersException.class)
    public void givenUserDataAndInvalidNameWhenExecuteIsInvokedThenThrowExeption()
            throws InvalidParametersException {

        final String firstName = "Ex ample";
        final String[] userData = {COMMAND, USERNAME_PARAMETER, USERNAME,
                PASSWORD_PARAMETER, PASSWORD, FIRST_NAME_PARAMETER, firstName,
                LAST_NAME_PARAMETER, LAST_NAME, EMAIL_PARAMETER, EMAIL};
        registerCommand.execute(users, userData);
    }

    @Test(expected = InvalidParametersException.class)
    public void givenUserDataAndInvalidEmailWhenExecuteIsInvokedThenThrowExeption()
            throws InvalidParametersException {

        final String email = "example@abv,bg";
        final String[] userData = {COMMAND, USERNAME_PARAMETER, USERNAME,
                PASSWORD_PARAMETER, PASSWORD, FIRST_NAME_PARAMETER, FIRST_NAME,
                LAST_NAME_PARAMETER, LAST_NAME, EMAIL_PARAMETER, email};
        registerCommand.execute(users, userData);
    }

}
