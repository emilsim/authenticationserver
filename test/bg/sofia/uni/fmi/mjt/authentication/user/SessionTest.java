package bg.sofia.uni.fmi.mjt.authentication.user;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class SessionTest {

    @Test
    public void givenNotExpiredSessionWhenIsSessionExpiredIsInvokedThenReturnFalse() {

        final String username = "example";
        final int timeToLive = 10;
        Session session = new Session(username, timeToLive);
        assertFalse(session.isSessionExpired());
    }

    @Test
    public void givenExpiredSessionWhenIsSessionExpiredIsInvokedThenReturnTrue() {

        final String username = "example";
        final int timeToLive = -1;
        Session session = new Session(username, timeToLive);
        assertTrue(session.isSessionExpired());
    }

}
