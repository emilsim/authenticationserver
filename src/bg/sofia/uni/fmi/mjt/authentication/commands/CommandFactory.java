package bg.sofia.uni.fmi.mjt.authentication.commands;

import java.util.HashMap;
import java.util.Map;

public class CommandFactory {

    private Map<String, Command> commands;

    public CommandFactory() {

        commands = new HashMap<>();
        commands.put("register", new RegisterCommand());
        commands.put("delete-user", new DeleteUserCommand());
        commands.put("login", new LoginCommand());
        commands.put("logout", new LogoutCommand());
        commands.put("reset-password", new ResetPasswordCommand());
        commands.put("update-user", new UpdateUserCommand());
    }

    public Command getCommand(String command) {

        return commands.get(command);
    }

}
