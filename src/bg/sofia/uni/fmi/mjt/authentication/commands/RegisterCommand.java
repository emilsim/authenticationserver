package bg.sofia.uni.fmi.mjt.authentication.commands;

import bg.sofia.uni.fmi.mjt.authentication.exceptions.InvalidParametersException;
import bg.sofia.uni.fmi.mjt.authentication.user.Users;

public class RegisterCommand implements Command {

    private static final int NUMBER_OF_PARAMETERS = 11;
    private static final int USERNAME_PARAMETER = 2;
    private static final int PASSWORD_PARAMETER = 4;
    private static final int FIRST_NAME_PARAMETER = 6;
    private static final int LAST_NAME_PARAMETER = 8;
    private static final int EMAIL_PARAMETER = 10;

    @Override
    public String execute(Users users, String[] tokens)
            throws InvalidParametersException {

        if (areParametersValid(tokens)) {
            if (users.isUserRegistered(tokens[USERNAME_PARAMETER])) {
                return "User " + tokens[USERNAME_PARAMETER]
                        + " is already registered";
            }
            users.addUser(tokens[USERNAME_PARAMETER],
                          tokens[PASSWORD_PARAMETER],
                          tokens[FIRST_NAME_PARAMETER],
                          tokens[LAST_NAME_PARAMETER], tokens[EMAIL_PARAMETER]);
        } else {
            return "You have entered an invalid parameters!";
        }
        return "You have rgistered successfully!";
    }

    private boolean areParametersValid(String[] tokens)
            throws InvalidParametersException {

        if (tokens.length != NUMBER_OF_PARAMETERS) {
            throw new InvalidParametersException(
                    "You have entered invalid number of parameters");
        }
        if (isUsernameValid(tokens[USERNAME_PARAMETER])
                && isPasswordValid(tokens[PASSWORD_PARAMETER])
                && isNameValid(tokens[FIRST_NAME_PARAMETER],
                               tokens[LAST_NAME_PARAMETER])
                && isEmailValid(tokens[EMAIL_PARAMETER])) {
            return true;
        }
        return false;
    }

    private boolean isEmailValid(String email)
            throws InvalidParametersException {
        if (email.matches("[a-zA-Z_0-9]+@[A-Za-z0-0_]+\\.[a-zA-Z0-0]+")) {
            return true;
        }
        throw new InvalidParametersException(
                "You have entered an invalid email!");
    }

    private boolean isNameValid(String firstName, String lastName)
            throws InvalidParametersException {

        if (firstName.matches("[A-Za-z]+") && lastName.matches("[A-Za-z]+")) {
            return true;
        }
        throw new InvalidParametersException(
                "You have entered an invalid name!");
    }

    private boolean isPasswordValid(String password)
            throws InvalidParametersException {

        if (password.matches("[A-Za-z0-9]+") && password.length() > 6) {
            return true;
        }
        throw new InvalidParametersException(
                "You have entered invalid password!"
                        + "(it should contain small and capital letters and numbers)");
    }

    private boolean isUsernameValid(String username)
            throws InvalidParametersException {

        if (username.matches("[a-zA-Z_]+") && username.length() > 3) {
            return true;
        }
        throw new InvalidParametersException(
                "You have entered invalid username!");
    }

}
