package bg.sofia.uni.fmi.mjt.authentication;

public class AuthenticationClientDemo {

    public static void main(String[] args) {

        InputScanner inputScanner = new InputScanner();
        AuthenticationClient client = new AuthenticationClient(inputScanner);
        client.run();
    }

}
