package bg.sofia.uni.fmi.mjt.authentication.enums;

public enum CommandType {

    REGISTER("register"), LOGIN("login"), RESET_PASSWORD("reset-password"), 
    UPDATE_USER("update-user"), LOGOUT("logout"), DELETE_USER("delete-user"), 
    DISCONNECT("disconnect");

    private String command;

    private CommandType(String command) {

        this.command = command;
    }

    public String getText() {

        return command;
    }

}
