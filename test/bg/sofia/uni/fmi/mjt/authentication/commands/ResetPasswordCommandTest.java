package bg.sofia.uni.fmi.mjt.authentication.commands;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import bg.sofia.uni.fmi.mjt.authentication.exceptions.InvalidParametersException;
import bg.sofia.uni.fmi.mjt.authentication.user.Users;

import org.junit.Before;
import org.junit.Test;

public class ResetPasswordCommandTest {

    private static final String COMMAND = "register";
    private static final String USERNAME_PARAMETER = "--username";
    private static final String OLD_PASSWORD_PARAMETER = "--old-password";
    private static final String NEW_PASSWORD_PARAMETER = "--new-password";
    private static final String USERNAME = "example";
    private static final String OLD_PASSWORD = "exa0mple";
    private static final String NEW_PASSWORD = "exa0mple";
    private Users users;
    private ResetPasswordCommand resetPasswordCommand;

    @Before
    public void setup() {
        users = mock(Users.class);
        resetPasswordCommand = new ResetPasswordCommand();
    }

    @Test
    public void givenUsernameAndPasswordsWhenExecuteIsInvokedThenResetPasswordToUser()
            throws InvalidParametersException {

        final String[] tokens = {COMMAND, USERNAME_PARAMETER, USERNAME,
                OLD_PASSWORD_PARAMETER, OLD_PASSWORD, NEW_PASSWORD_PARAMETER,
                NEW_PASSWORD};
        resetPasswordCommand.execute(users, tokens);
        final int numberOfInvokes = 1;
        verify(users, times(numberOfInvokes)).resetPassword(USERNAME,
                                                            OLD_PASSWORD,
                                                            NEW_PASSWORD);
    }

    @Test(expected = InvalidParametersException.class)
    public void givenInvalidNumberOfParametersWhenExecuteIsInvokedThenThrowException()
            throws InvalidParametersException {

        final String[] tokens = {COMMAND, USERNAME, OLD_PASSWORD};
        resetPasswordCommand.execute(users, tokens);
    }

}
