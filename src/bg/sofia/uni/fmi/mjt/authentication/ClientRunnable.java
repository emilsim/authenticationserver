package bg.sofia.uni.fmi.mjt.authentication;

import bg.sofia.uni.fmi.mjt.authentication.exceptions.ExceptionLogger;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.Socket;

public class ClientRunnable implements Runnable {

    private Socket socket;
    private BufferedReader socketReader;
    private boolean isDisconnected;

    public ClientRunnable(Socket socket, BufferedReader socketReader) {

        this.socketReader = socketReader;
        this.socket = socket;
        this.isDisconnected = false;
    }

    public void disconnect() {

        this.isDisconnected = true;
    }

    @Override
    public void run() {

        while (true) {
            if (isDisconnected || socket.isClosed()) {
                System.out.println("client socket is closed, stop waiting for server messages");
                return;
            }
            try {
                System.out.println(socketReader.readLine());
            } catch (IOException exception) {
                System.out.println("Cannot read from the socket!");
                ExceptionLogger.logException(exception);
            }
        }
    }

}
