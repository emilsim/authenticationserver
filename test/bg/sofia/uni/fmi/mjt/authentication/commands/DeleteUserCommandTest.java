package bg.sofia.uni.fmi.mjt.authentication.commands;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import bg.sofia.uni.fmi.mjt.authentication.exceptions.InvalidParametersException;
import bg.sofia.uni.fmi.mjt.authentication.user.Users;

import org.junit.Before;
import org.junit.Test;

public class DeleteUserCommandTest {

    private static final String COMMAND = "delete-user";
    private static final String USERNAME_PARAMETER = "-username";
    private Users users;
    private DeleteUserCommand deleteUserCommand;

    @Before
    public void setup() {

        users = mock(Users.class);
        deleteUserCommand = new DeleteUserCommand();
    }

    @Test
    public void givenUserThatExistsWhenExecuteIsInvokedThenRemoveUser()
            throws InvalidParametersException {

        final String username = "example";
        final String[] tokens = {COMMAND, USERNAME_PARAMETER, username};
        when(users.isUserRegistered(username)).thenReturn(true);
        deleteUserCommand.execute(users, tokens);
        final int numberOfTimes = 1;
        verify(users, times(numberOfTimes)).removeUser(username);
    }

    @Test
    public void givenUserThatNotExistsWhenExecuteIsInvokedThenReturnMessage()
            throws InvalidParametersException {

        final String username = "example";
        final String[] tokens = {COMMAND, USERNAME_PARAMETER, username};
        when(users.isUserRegistered(username)).thenReturn(false);
        String result = deleteUserCommand.execute(users, tokens);
        String expected = "User " + username + " is not existing!";
        assertEquals(expected, result);
    }

    @Test(expected = InvalidParametersException.class)
    public void givenInvalidNumberOfParametersWhenExecuteIsInvokedThenThrowException()
            throws InvalidParametersException {

        final String[] tokens = {COMMAND};
        deleteUserCommand.execute(users, tokens);
    }

}
