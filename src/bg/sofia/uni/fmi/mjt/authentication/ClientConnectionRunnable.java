package bg.sofia.uni.fmi.mjt.authentication;

import bg.sofia.uni.fmi.mjt.authentication.commands.Command;
import bg.sofia.uni.fmi.mjt.authentication.commands.CommandFactory;
import bg.sofia.uni.fmi.mjt.authentication.exceptions.ExceptionLogger;
import bg.sofia.uni.fmi.mjt.authentication.exceptions.InvalidParametersException;
import bg.sofia.uni.fmi.mjt.authentication.user.Users;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientConnectionRunnable implements Runnable {

    private static final int COMMAND_PARAMETER = 0;
    private Socket socket;
    private Users users;
    private CommandFactory commandFactory;

    public ClientConnectionRunnable(Socket socket, Users users,
            CommandFactory commandFactory) {

        this.socket = socket;
        this.users = users;
        this.commandFactory = commandFactory;
    }

    @Override
    public void run() {

        try (BufferedReader socketReader = new BufferedReader(
                new InputStreamReader(socket.getInputStream()));
                PrintWriter socketWriter = new PrintWriter(
                        socket.getOutputStream(), true);) {
            while (true) {
                String clientInput = socketReader.readLine();
                executeCommand(clientInput, socketWriter);
            }

        } catch (IOException exception) {
            ExceptionLogger.logException(exception);
            throw new RuntimeException("Cannot get socket streams");
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                System.out.println("Cannot close the socket!");
            }
        }
    }

    private void executeCommand(String clientInput, PrintWriter socketWriter) {

        String[] tokens = clientInput.split(" ");
        String textCommand = tokens[COMMAND_PARAMETER];
        Command command = commandFactory.getCommand(textCommand);
        if (command != null) {
            String result = null;
            try {
                result = command.execute(users, tokens);
                socketWriter.println(result);
            } catch (InvalidParametersException exception) {
                socketWriter.println(exception.getMessage());
            }
        } else {
            socketWriter.println("Invalid command " + textCommand);
        }
    }
}
