package bg.sofia.uni.fmi.mjt.authentication.exceptions;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

public class ExceptionLogger {

    private static final String LOGGING_FILE = "resources/logs/exceptions.txt";

    public static synchronized void logException(IOException exception) {

        try (OutputStream out = new FileOutputStream(LOGGING_FILE);
                PrintStream printStream = new PrintStream(out)) {
            printStream.println(exception);
            exception.printStackTrace(printStream);
        } catch (IOException writerException) {
            System.out.println("Cannot write exception in file "
                    + LOGGING_FILE);
        }

    }

}
