package bg.sofia.uni.fmi.mjt.authentication.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import org.junit.Before;
import org.junit.Test;

public class UsersTest {

    private static final String FILE_NAME = "users/users.bin";
    private Users users;

    @Before
    public void setup() {

        this.users = new Users();
        final String username = "example";
        final String password = "example";
        final String firstName = "firstName";
        final String lastName = "lastName";
        final String email = "example@mail.bg";
        users.addUser(username, password, firstName, lastName, email);
    }

    @Test
    public void givenUserWhenSaveUsersToFileIsInvokedThenSaveUserToFile()
            throws FileNotFoundException, IOException, ClassNotFoundException {

        users.saveUsersToFile();
        final String username = "example";
        ObjectInputStream objectInputStream = new ObjectInputStream(
                new FileInputStream(FILE_NAME));
        int numberOfUsers = objectInputStream.readInt();
        User user = (User) objectInputStream.readObject();
        assertEquals(user.getUsername(), username);
    }

    @Test
    public void givenUserInFileWhenReadUsersFromFileIsInvokedThenReadUserFromFile() {

        final String username = "example";
        users.saveUsersToFile();
        Users users = new Users();
        users.readUsersFromFile();
        users.isUserRegistered(username);
    }

    @Test
    public void givenUserDataWhenAddUserIsInvokedTheAddUserToTheRegisteredUsers() {

        final String username = "user";
        final String password = "user";
        final String firstName = "firstName";
        final String lastName = "lastName";
        final String email = "example@mail.bg";
        users.addUser(username, password, firstName, lastName, email);
        assertTrue(users.isUserRegistered(username));
    }

    @Test
    public void givenUserThatExistsInUsersWhenRemoveUserIsInvokedThenRemoveThatUser() {

        final String username = "example";
        users.removeUser(username);
        assertFalse(users.isUserRegistered(username));
    }

    @Test
    public void givenExistingUsernameAndValidPasswordWhenAuthenticateUserIsInvokedThenAuthenticateUser() {

        final String username = "example";
        final String password = "example";
        users.authenticateUser(username, password);
        assertTrue(users.isUserAuthenticated(username));
    }

    @Test
    public void givenNotExistingUsernameWhenAuthenticateUserIsInvokedThenReturnErrorMessage() {

        final String username = "Example";
        final String password = "example";
        String result = users.authenticateUser(username, password);
        String expected = "You have entered an invalid username";
        assertEquals(expected, result);
    }

    @Test
    public void givenExistingUsernameAndInvalidPasswordWhenAuthenticateUserIsInvokedThenReturnErrorMessage() {

        final String username = "example";
        final String password = "Example";
        String result = users.authenticateUser(username, password);
        String expected = "You have entered an invalid password";
        assertEquals(expected, result);
    }

    @Test
    public void givenExistingUsernameAndValidPasswordWhenAuthenticateUserIsInvokedTwoTimesThenAuthenticateUser() {

        final String username = "example";
        final String password = "example";
        users.authenticateUser(username, password);
        users.authenticateUser(username, password);
        assertTrue(users.isUserAuthenticated(username));
    }

    @Test
    public void givenInvalidSessionIdWhenAuthenticateUserIsInvokedThenReturnErrorMessage() {

        final String sessionId = "123";
        String result = users.authenticateUser(sessionId);
        String expected = "Session id: " + sessionId + " doesn't exist!";
        assertEquals(expected, result);
    }

    @Test
    public void givenInvalidSessionIdWhenDeauthenticateUserIsInvokedThenReturnErrorMessage() {

        final String sessionId = "123";
        String result = users.deauthenticateUser(sessionId);
        String expected = "Session id: " + sessionId + " doesn't exist!";
        assertEquals(expected, result);
    }

    @Test
    public void givenNotLoggedInUserWhenResetPasswordIsInvokedThenReturnErrorMessage() {

        final String username = "notValid";
        final String oldPassword = "a123456";
        final String newPassword = "b123567";
        String result = users.resetPassword(username, oldPassword, newPassword);
        String expected = "You should loggin into the system to change password!";
        assertEquals(expected, result);
    }

    @Test
    public void givenLoggedInUserAndInvalidOldPasswordWhenResetPasswordIsInvokedThenReturnErrorMessage() {

        final String username = "example";
        final String password = "example";
        final String oldPassword = "a123456";
        final String newPassword = "b123567";
        users.authenticateUser(username, password);
        String result = users.resetPassword(username, oldPassword, newPassword);
        String expected = "You have entered an invalid old password!";
        assertEquals(expected, result);
    }

    @Test
    public void givenLoggedInUserAndValidPasswordsWhenResetPasswordIsInvokedThenResetPassword() {

        final String username = "example";
        final String password = "example";
        final String newPassword = "b123567";
        users.authenticateUser(username, password);
        String result = users.resetPassword(username, password, newPassword);
        String expected = "Successfully change password!";
        assertEquals(expected, result);
    }

}
