package bg.sofia.uni.fmi.mjt.authentication.user;

import java.time.OffsetDateTime;
import java.util.UUID;

public class Session {
    private final String uniqueId;
    private OffsetDateTime timeToLive;
    private final String username;

    public Session(String username, int timeToLive) {

        this.uniqueId = UUID.randomUUID().toString();
        this.timeToLive = OffsetDateTime.now().plusMinutes(timeToLive);
        this.username = username;
    }

    public boolean isSessionExpired() {

        return timeToLive.isBefore(OffsetDateTime.now()) ? true : false;
    }

    public String getUsername() {

        return username;
    }

    public String getUniqueId() {

        return uniqueId;
    }

    public String getTimeToLive() {

        return timeToLive.toString();
    }
}
