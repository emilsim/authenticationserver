package bg.sofia.uni.fmi.mjt.authentication.commands;

import bg.sofia.uni.fmi.mjt.authentication.exceptions.InvalidParametersException;
import bg.sofia.uni.fmi.mjt.authentication.user.Users;

public class LogoutCommand implements Command {

    private static final int NUMBER_OF_PARAMETERS = 3;
    private static final int SESSION_PARAMETER = 2;

    @Override
    public String execute(Users users, String[] tokens)
            throws InvalidParametersException {

        if (areParametersValid(tokens)) {
            return users.deauthenticateUser(tokens[SESSION_PARAMETER]);
        }
        return "You have entered invalid parameters";
    }

    private boolean areParametersValid(String[] tokens)
            throws InvalidParametersException {

        if (tokens.length == NUMBER_OF_PARAMETERS) {
            return true;
        }
        throw new InvalidParametersException(
                "You have entered invalid number of parameters!");
    }

}
