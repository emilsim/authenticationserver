package bg.sofia.uni.fmi.mjt.authentication.user;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import bg.sofia.uni.fmi.mjt.authentication.exceptions.InvalidPasswordException;

import org.junit.Before;
import org.junit.Test;

public class UserTest {

    private static final String USERNAME = "example";
    private static final String PASSWORD = "example";
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String EMAIL = "example@example.com";
    private User user;

    @Before
    public void setup() {

        user = new User(USERNAME, PASSWORD, FIRST_NAME, LAST_NAME, EMAIL);
    }

    @Test
    public void givenUserAndValidPasswordWhenIsValidPasswordIsInvokedThenReturnTrue() {

        assertTrue(user.isValidPassword(PASSWORD));
    }

    @Test
    public void givenUserAndInvalidPasswordWhenIsValidPasswordIsInvokedThenReturnFalse() {

        final String password = "newPassword";
        assertFalse(user.isValidPassword(password));
    }

    @Test
    public void givenValidOldPasswordAndNewPasswordWhenSetPasswordIsInvokedThenSetTheNewPassword()
            throws InvalidPasswordException {

        final String newPassword = "newPassword";
        user.setPassword(newPassword);
        assertTrue(user.isValidPassword(newPassword));
    }
}
