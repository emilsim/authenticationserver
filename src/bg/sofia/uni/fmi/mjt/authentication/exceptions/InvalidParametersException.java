package bg.sofia.uni.fmi.mjt.authentication.exceptions;

public class InvalidParametersException extends Exception {

    private static final long serialVersionUID = 9094588476218345850L;

    public InvalidParametersException(String message) {

        super(message);
    }

}
