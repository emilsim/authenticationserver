package bg.sofia.uni.fmi.mjt.authentication;

import java.util.Scanner;

public class InputScanner {

    private final Scanner scanner;

    public InputScanner() {

        this(new Scanner(System.in));
    }

    InputScanner(Scanner scanner) {

        this.scanner = scanner;
    }

    public String getString() {

        return scanner.nextLine();
    }

    public String getString(String text) {

        System.out.print(text + ":");
        return scanner.nextLine();
    }

    public int getInt(String text) {

        System.out.print(text + ":");
        return Integer.parseInt(scanner.nextLine());
    }

    public int getInt() {

        return Integer.parseInt(scanner.nextLine());
    }

}
