package bg.sofia.uni.fmi.mjt.authentication.commands;

import bg.sofia.uni.fmi.mjt.authentication.exceptions.InvalidParametersException;
import bg.sofia.uni.fmi.mjt.authentication.user.Users;

public class DeleteUserCommand implements Command {

    private static final int NUMBER_OF_PARAMETERS = 3;
    private static final int USERNAME_PARAMETER = 2;

    @Override
    public String execute(Users users, String[] tokens)
            throws InvalidParametersException {

        String username = null;
        if (areParametersValid(tokens)) {
            username = tokens[USERNAME_PARAMETER];
            if (users.isUserRegistered(username)) {
                users.removeUser(username);
            } else {
                return "User " + username + " is not existing!";
            }
        } else {
            return "You have entered invalid parameters!";
        }
        return "Successfully remove user " + username;
    }

    private boolean areParametersValid(String[] tokens)
            throws InvalidParametersException {

        if (tokens.length != NUMBER_OF_PARAMETERS) {
            throw new InvalidParametersException(
                    "You have entered invalid number of parameters");
        }
        return true;
    }

}
