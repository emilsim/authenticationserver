package bg.sofia.uni.fmi.mjt.authentication;

import bg.sofia.uni.fmi.mjt.authentication.commands.CommandFactory;
import bg.sofia.uni.fmi.mjt.authentication.exceptions.ExceptionLogger;
import bg.sofia.uni.fmi.mjt.authentication.user.Users;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class AuthenticationServer {

    private static final int PORT = 8080;
    private Users users;
    private CommandFactory commandFactory;

    public AuthenticationServer() {
        users = new Users();
        users.readUsersFromFile();
        commandFactory = new CommandFactory();
    }

    public void start() {

        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            while (true) {
                Socket socket = serverSocket.accept();
                ClientConnectionRunnable runnable = new ClientConnectionRunnable(
                        socket, users, commandFactory);
                new Thread(runnable).start();
            }
        } catch (IOException exception) {
            ExceptionLogger.logException(exception);
            throw new IllegalStateException(
                    "Cannot create new socket on port " + PORT);
        } finally {
            users.saveUsersToFile();
        }
    }

}
